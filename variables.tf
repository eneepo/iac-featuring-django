# Europe (Frankfurt)
variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "eu-central-1"
}

# Amazon Linux 2 AMI (HVM)
variable "aws_amis" {
  default = {
    "eu-central-1" = "ami-0ec1ba09723e5bfac"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "project_name" {
  type = string
}

variable "project_description" {
  type = string
}

variable "project_domain" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_pass" {
  type = string
}