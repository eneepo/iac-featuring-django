resource "aws_s3_bucket" "static" {
  bucket = "static.${var.project_domain}"
  acl    = "public-read"

  cors_rule {
    allowed_headers = ["Authorization"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = [
      "https://static.${var.project_domain}",
      "https://${var.project_domain}"
    ]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Terraform   = "true"
  }
}

resource "aws_s3_bucket" "media" {
  bucket = "media.${var.project_domain}"
  acl    = "public-read"

  cors_rule {
    allowed_headers = ["Authorization"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = [
      "https://media.${var.project_domain}",
      "https://${var.project_domain}"
    ]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Terraform   = "true"
  }
}

resource "aws_iam_role_policy" "assets_policy" {
    name = "assets-policy"
    role = aws_iam_role.role.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "s3:GetObject"
      ],
      "Resource": [
        "${aws_s3_bucket.static.arn}/*",
        "${aws_s3_bucket.media.arn}/*"
      ]
    }
  ]
}
POLICY
}

data "aws_route53_zone" "zone" {
  name = "${var.project_domain}."
}

resource "aws_route53_record" "static" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "static.${var.project_domain}"
  type    = "A"

  alias {
    name                   = "s3-website.${var.aws_region}.amazonaws.com."
    zone_id                = aws_s3_bucket.static.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "media" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "media.${var.project_domain}"
  type    = "A"

  alias {
    name                   = "s3-website.${var.aws_region}.amazonaws.com."
    zone_id                = aws_s3_bucket.media.hosted_zone_id
    evaluate_target_health = true
  }
}
