# IaC featuring Django
This Infrastructure as Code uses Terraform and allows you to spin up production envirnoment in AWS. Also it uses Continuous Integration and Continuous Deployment techniques while minimizing the introduction of human errors after the development stage. 

## This IaC spins up an environment with the following AWS services
- IAM - Creates user, roles and policies
- RDS - Creates PostgreSQL Database
- S3 - Creates two S3 Bucket, one for assets(static and media files) and another for the pipeline
- SSM(Parameter store) - Stores secrects, DB creds, etc 
- CI/CD:
    * CodeCommit: Creates git repository
    * CodeBuild: Tests and colects the static files
    * CodePipeline: Deploying the code if tests passed
- Route53: Creating required records for a fully functional web application

## Instructions

Rename `terraform.tfvars.template` to `terraform.tfvars` and assign your settings to the defined variables and apply the code using terraform.

```sh
terraform apply
```

