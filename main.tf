provider "aws" {
  region = var.aws_region
}

resource "aws_iam_user" "bucket_user" {
  name = "${var.project_name}-bucket-user"

  tags = {
    Terraform = "true"
  }
}

resource "aws_iam_role" "role" {
  name = "${var.project_name}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

module "rds" {
  source        = "./modules/rds"

  instance_type = var.instance_type
  db_name       = var.db_name
  db_user       = var.db_user
  db_pass       = var.db_pass
}

module "codesuite" {
  source              = "./modules/codesuite"

  project_name        = var.project_name
  project_description = var.project_description
  project_domain      = var.project_domain
  aws_region          = var.aws_region

  aws_iam_role        = aws_iam_role.role
}