output "db_address" {
  value = module.rds.db_address
}

output "git_clone_url_http" {
  value = module.codesuite.git_clone_url_http
}

output "git_clone_url_ssh" {
  value = module.codesuite.git_clone_url_ssh
}
