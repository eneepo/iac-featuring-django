resource "aws_db_instance" "this" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "9.6.16"
  instance_class       = "db.${var.instance_type}"
  name                 = var.db_name
  username             = var.db_user
  password             = var.db_pass

  tags = {
    Terraform   = "true"
  }
}
