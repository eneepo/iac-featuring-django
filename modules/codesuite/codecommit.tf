resource "aws_codecommit_repository" "this" {
  repository_name = var.project_name
  description     = var.project_description

  lifecycle {
    prevent_destroy = false
  }

  tags = {
    Terraform   = "true"
  }
}

resource "aws_iam_role_policy" "codecommit_policy" {
  name = "codecommit-policy"
  role = var.aws_iam_role.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "codecommit:CancelUploadArchive",
        "codecommit:GetBranch",
        "codecommit:GetCommit",
        "codecommit:GetUploadArchiveStatus",
        "codecommit:UploadArchive"
      ],
      "Resource": [
        "${aws_codecommit_repository.this.arn}",
        "${aws_codecommit_repository.this.arn}/*"
      ]
    }
  ]
}
POLICY
}
