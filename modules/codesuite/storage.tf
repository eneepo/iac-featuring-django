resource "aws_s3_bucket" "this" {
  bucket = "${var.project_name}-pipeline-bucket"
  acl    = "private"

  tags = {
    Terraform   = "true"
  }
}

resource "aws_iam_role_policy" "storage_policy" {
    name = "storage-policy"
    role = var.aws_iam_role.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketVersioning",
        "s3:PutObject"
      ],
      "Resource": [
        "${aws_s3_bucket.this.arn}",
        "${aws_s3_bucket.this.arn}/*"
      ]
    }
  ]
}
POLICY
}
