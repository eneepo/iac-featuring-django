variable "aws_iam_role" {
  type    = any
  default = null
}

variable "aws_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_description" {
  type = string
}

variable "project_domain" {
  type = string
}
